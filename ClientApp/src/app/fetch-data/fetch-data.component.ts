import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public users: SampUser[];

  private gridApi;
  private gridColumnApi;

  private columnDefs;
  private components;
  private rowData;
  private editType;
  private editingRowIndex;

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    this.columnDefs = [
      { headerName: 'Name', field: 'name', editable: true, sortable: true },
      { headerName: 'Job Title', field: 'jobTitle', editable: true, sortable: true },
      { headerName: 'Date Started', field: 'dateStarted', editable: true, sortable: true },
      { headerName: 'Office Location', field: 'officeLoction', editable: true, sortable: true }
    ];
    
    http.get<SampUser[]>(baseUrl + 'api/SampUsers/').subscribe(result => {
      this.rowData = JSON.parse(JSON.stringify(result));
    }, error => console.error(error));
  }

  onCellClicked($event) {
    // check whether the current row is already opened in edit or not
    if (this.editingRowIndex != $event.rowIndex) {
      console.log($event);
      $event.api.startEditingCell({
        rowIndex: $event.rowIndex,
        colKey: $event.column.colId
      });
      this.editingRowIndex = $event.rowIndex;
    }
  }

  onCellValueChanged(params: any) {
    if (params) {
      let id = params.data.id;
      let column = params.colDef.field;
      let value = params.newValue;
      this.http.post(this.baseUrl + 'api/SampUsers', params.data).subscribe(result => {
        console.log(result);
      })
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    params.api.sizeColumnsToFit();
  }
  

}

interface SampUser {
  name: string;
  jobTitle: string;
  dateStarted: string;
  officeLocation: string;
}
