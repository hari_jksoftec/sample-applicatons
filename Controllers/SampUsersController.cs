﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sampapp.EF;
using sampapp.Models;

namespace samp_app.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SampUsersController : ControllerBase
    {
        private readonly DataContext _context;

        public SampUsersController(DataContext context)
        {
            _context = context;
        }

        // GET: api/SampUsers
        [HttpGet]
        public IEnumerable<SampUser> GetSampUser()
        {
            return _context.SampUser;
        }

        // GET: api/SampUsers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSampUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sampUser = await _context.SampUser.FindAsync(id);

            if (sampUser == null)
            {
                return NotFound();
            }

            return Ok(sampUser);
        }

        // PUT: api/SampUsers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSampUser([FromRoute] int id, [FromBody] SampUser sampUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sampUser.Id)
            {
                return BadRequest();
            }

            _context.Entry(sampUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SampUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SampUsers
        [HttpPost]
        public async Task<IActionResult> PostSampUser([FromBody] SampUser sampUser)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //_context.SampUser.Add(sampUser);
            //await _context.SaveChangesAsync();

            //return CreatedAtAction("GetSampUser", new { id = sampUser.Id }, sampUser);

            _context.SampUser.Update(sampUser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSampUser", new { id = sampUser.Id }, sampUser);
        }
        

        // DELETE: api/SampUsers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSampUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sampUser = await _context.SampUser.FindAsync(id);
            if (sampUser == null)
            {
                return NotFound();
            }

            _context.SampUser.Remove(sampUser);
            await _context.SaveChangesAsync();

            return Ok(sampUser);
        }

        private bool SampUserExists(int id)
        {
            return _context.SampUser.Any(e => e.Id == id);
        }
    }
}