﻿using Microsoft.EntityFrameworkCore;
using sampapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sampapp.EF
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<SampUser> SampUser { get; set; }
    }
}
