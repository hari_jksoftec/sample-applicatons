﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sampapp.Models
{
    public class SampUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string JobTitle { get; set; }
        public string DateStarted { get; set; }
        public string OfficeLoction { get; set; }
    }
}
